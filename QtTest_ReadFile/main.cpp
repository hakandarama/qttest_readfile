#include <QFile>
#include <iostream>
#include <QTextStream>
#include <QDir>
#include <qdebug.h>
#include <QString>
#include <QIODevice>


int main()
{
    QFile file_taskList("../../../../dosyalar/taskList");
    QFile file_taskIndex("../../../../dosyalar/taskIndex");
    QFile file_taskFile("../../../../dosyalar/taskFile");

    if(!file_taskList.open(QIODevice::ReadOnly | QIODevice::Unbuffered ))
    {
        std::cout << "taskList dosyasi acilamadi";
        std::cout << file_taskList.errorString().toStdString();
        std::cout << file_taskList.error();
    }
    uchar *taskList = file_taskList.map(0, file_taskList.size());

    if(!file_taskIndex.open(QIODevice::ReadOnly | QIODevice::Unbuffered ))
    {
        std::cout << "taskIndex dosyasi acilamadi";
        std::cout << file_taskIndex.errorString().toStdString();
        std::cout << file_taskIndex.error();
    }
    uchar *taskIndex = file_taskIndex.map(0, file_taskIndex.size());

    if(!file_taskFile.open(QIODevice::ReadOnly | QIODevice::Unbuffered ))
    {
        std::cout << "taskFile dosyasi acilamadi";
        std::cout << file_taskFile.errorString().toStdString();
        std::cout << file_taskFile.error();
    }
    uchar *taskFile = file_taskFile.map(0, file_taskFile.size());

    char ch = ' ';
    QByteArray chList = QByteArray();

    qDebug() << "Dosya boyutu" << file_taskList.size();
    if (taskList) {
        for (int i = 0; i < file_taskList.size();i++) {
            ch = static_cast<char>(taskList[i]);
            if (ch == ' ')
            {
                qint32 indexPeeker = chList.toInt() * 8;

                qint32 code =
                        taskIndex[indexPeeker+7] |
                        taskIndex[indexPeeker+6] |
                        taskIndex[indexPeeker+5] |
                        taskIndex[indexPeeker+4];
                qint32 address =
                        taskIndex[indexPeeker+3] |
                        taskIndex[indexPeeker+2] |
                        taskIndex[indexPeeker+1] |
                        taskIndex[indexPeeker];

                chList.clear();

            }
            else
            {
                chList.append(ch);
            }
        }
    file_taskList.unmap(taskList);
    }
    file_taskList.close();
}

